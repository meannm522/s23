// db.collectionName.insertOne({ object })

db.rooms.insert({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false 
    })

// insert many
db.rooms.insertMany([
{
    name: "Double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for small family going on a vacation",
    rooms_available: 5,
    isAvailable: false
    },
    {
    name: "Queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false 
    }

 // Update Queen to 0

 db.rooms.updateOne({name: "Queen"},{ $set : {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 0,
        isAvailable: false 
        }
    });

 // delete Many()

 db.rooms.deleteMany({rooms_available: 0})